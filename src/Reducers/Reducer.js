const reducer = (state = {}, action) => {
    switch (action.type) {
      case 'GET_DATA':
        return { ...state};
      case 'DATA_RECEIVED':
        return { ...state, list: action.json}
      default:
        return state;
    }
  };
  
  export default reducer;