import axios from 'axios'
import { put, takeLatest, all } from 'redux-saga/effects';

function* fetchData() {
    const getData = axios.get('https://jsonplaceholder.typicode.com/comments')
        .then(response => {
            console.log(response);
        })
        .catch(error => {
            console.log(error);
        });

  yield put({ type: "DATA_RECEIVED", json: getData.data || [] });
}

function* actionWatcher() {
  yield takeLatest('GET_DATA', fetchData)
}


export default function* rootSaga() {
  yield all([
    actionWatcher(),
  ]);
}