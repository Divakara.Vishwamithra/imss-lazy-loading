import React, { useState, useEffect } from 'react';
import MainComponent from './Components/Onscroll';
import { useDispatch, useSelector } from "react-redux";

function App() {
  
  const [data, setData] = useState([]);

  // const dataList = useSelector(state => state.list)
  // console.log(dataList)

  return (
    <div className='App'>
      <MainComponent state={data} setState={setData}/>
    </div>
  );
};

export default App;