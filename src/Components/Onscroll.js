import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import getData from '../Actions/Action'


const sendData = () => ({type:'GET_DATA'})

function MainComponent(props) {

  console.log(props)
  const [loadMore, setLoadMore] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    getData(loadMore);
    setLoadMore(false);
  }, [loadMore]);

  useEffect(() => {
    const list = document.getElementById('list')
    if(props.scrollable) {   
      list.addEventListener('scroll', (e) => {
        const el = e.target;
        if(el.scrollTop + el.clientHeight === el.scrollHeight) {
          setLoadMore(true);
        }
      });  
    } else {   
      window.addEventListener('scroll', () => {
        if (window.scrollY + window.innerHeight === list.clientHeight + list.offsetTop) {
          setLoadMore(true);
        }
      });
    }
  }, []);

  useEffect(() => {
    const list = document.getElementById('list');

    if(list.clientHeight <= window.innerHeight && list.clientHeight) {
      setLoadMore(true);
    }
  }, [props.state]);

  const getData = (load) => {
    if (load) {
      dispatch(sendData())
    }
  };


  return (
    <ul id='list'>
      {props.state.map((data, i) => <li key={i}> {data.name} </li>) }
    </ul>
  );
}

export default MainComponent;
